/* 
 * The MIT License
 *
 * Copyright 2013 Computing for Volunteer Welfare Organizations (CVWO),
 * National University of Singapore.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

(function($) {
	window.PartialDate = function(year, month) {
		this.getDate = function() {
			return 0;
		}
		this.getFullYear = function() {
			return year;
		}
		this.getMonth = function() {
			return month;
		}
		this.getYear = function() {
			return year % 100;
		}
	};
	
	window.PartialDate.handle = function(instid, year, month) {
		var inst = document.getElementById(instid);
		var format = jQuery.datepicker._get(jQuery.datepicker._getInst(inst), 'dateFormat');
		var date = jQuery.datepicker.formatDate(format, new PartialDate(year, month));
		jQuery.datepicker._selectDate('#' + instid, date);
	};

	var __generateHTML = $.datepicker._generateHTML;
	$.datepicker._generateHTML = function(inst) {
		var result = __generateHTML.apply(this, [inst]);

		//If we don't allow partial dates, don't bother with the hack below.
		if (!inst.settings.partial) {
			return result;
		}

		//Inject our select month/select year buttons.
		result += '<div id="ui-datepicker-partial"><div id="ui-datepicker-partial-year">\n\
	<a onclick="PartialDate.handle(\'' + inst.id + '\', ' + inst.drawYear + ', -1)">Pick Year</a>\n\
</div>\n\
<div id="ui-datepicker-partial-month">\n\
	<a onclick="PartialDate.handle(\'' + inst.id + '\', ' + inst.drawYear + ', ' +
				inst.drawMonth + ')">Pick Month</a></div>\n\
</div>';
		
		return result;
	}
})(jQuery);
